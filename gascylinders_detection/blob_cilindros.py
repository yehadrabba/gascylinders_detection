# Standard imports
import cv2
import numpy as np;
 
# Read image
image = "input.JPG"

img = cv2.imread(image)
#img = cv2.imread("abastible_maipu/DSC00747.JPG")

im = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
#im = cv2.imread("abastible_maipu/DSC00747.JPG", cv2.IMREAD_GRAYSCALE)

#im = cv2.blur(im, (5,5))


 # Setup SimpleBlobDetector parameters.
params = cv2.SimpleBlobDetector_Params()
 
# Change thresholds
#params.minThreshold = 10;
#params.maxThreshold = 200;
 
# Filter by Area.
params.filterByArea = True
params.minArea = 20
params.maxArea = 200

# Filter by Circularity
params.filterByCircularity = True
params.minCircularity = 0.01
 
# Filter by Convexity
params.filterByConvexity = True
params.minConvexity = 0.9
 
# Filter by Inertia
params.filterByInertia = True
params.minInertiaRatio = 0.01
 
# Create a detector with the parameters
ver = (cv2.__version__).split('.')
if int(ver[0]) < 3 :
    detector = cv2.SimpleBlobDetector(params)
else : 
    detector = cv2.SimpleBlobDetector_create(params)


# Set up the detector with default parameters.
#detector = cv2.SimpleBlobDetector()
 
# Detect blobs.
keypoints = detector.detect(im)
# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
im_with_keypoints = cv2.drawKeypoints(img, keypoints, np.array([]), (0,255,0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
 
# Show keypoints

cv2.namedWindow('Keypoints',cv2.WINDOW_NORMAL)
cv2.imshow("Keypoints", im_with_keypoints)

cv2.namedWindow('Original',cv2.WINDOW_NORMAL)
cv2.imshow("Original", img)
#imtowrite= image + "_blob"
#cv2.imwrite(imtowrite,im_with_keypoints)

cv2.waitKey(0)